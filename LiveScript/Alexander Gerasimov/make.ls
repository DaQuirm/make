make = (...xs, arg) ->
	if typeof arg isnt 'function'
		make xs, _
	else
		xs.reduce arg

console.log make 1, 2, 3, -4, (*)
