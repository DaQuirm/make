var make = function (value) {
    return (function (f, g) {
        return f(g, f);
    })(function (cont, loop) {
        return function (envy) {
            return (
                envy instanceof Function ?
                    cont(envy) :
                    loop(function (reducer) {
                        return reducer(envy, cont(reducer));
                    }, loop));
        };
    }, function () {
        return value;
    });
};
