function make(a) {
	var mas = [];
	mas.push(a);
	var ret = function(b) {
		if (typeof(b) === "function") {
			console.log(mas.reduce(function(acc, input) {
				acc = b(acc, input);
				return acc;
			}));
		} else {
			mas.push(b);
			return ret;
		}
	};
	return ret;
}
