function make() {
	var arg = arguments[arguments.length-1];
	if (typeof arg !== 'function') {
		return Function.prototype.bind.apply(
			make,
			[].concat.apply([null], arguments)
		);
	} else {
		return [].slice.call(arguments, 0, -1).reduce(arg);
	}
}

console.log(make(1)(2)(3)(4)(function(a,b){ return a*b }));
