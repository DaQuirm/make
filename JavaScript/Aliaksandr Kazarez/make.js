var make = (function() {
    function mk(arr, arg) {

        if (typeof arg === 'function') {
            var res = arr.reduce(arg);
            arr.length=0;
            return res;
        } else {
            arr.push(arg)
            return mk.bind(null, arr);
        }
    }

    return mk.bind(null, []);
})()

document.open();
var res = make(1)(22)(1)(18)(function(x,y) {return x+y})
document.write(res);
document.close();
