using System;
using System.Collections.Generic;
using System.Linq;

namespace Curry
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Func<dynamic, dynamic, dynamic> func = (x, y) => x + y;

            dynamic o = make(1)(2)(3)(func);

            dynamic a = make(1);
            dynamic b = make(-1);

            dynamic o1 = a(-2)(func);
            dynamic o2 = b(-3)(func);
        }

        private static dynamic make(dynamic obj, dynamic list = null)
        {
            List<dynamic> objects = list != null ? (List<dynamic>)list : new List<dynamic>();
            var func = obj as Func<dynamic, dynamic, dynamic>;
            if (func != null) return objects.Aggregate(func);
            objects.Add(obj);
            return (Func<dynamic, dynamic>)(o => make(o, objects));
        }
    }
}
