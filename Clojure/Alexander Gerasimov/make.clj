(defn make [& args]
  (let [arg (last args)]
    (if (clojure.test/function? arg)
      (reduce arg (butlast args))
      (apply partial make args))))


(((((make 1) 2) 3) -4) *)